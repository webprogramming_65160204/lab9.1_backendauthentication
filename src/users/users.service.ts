import { Injectable } from '@nestjs/common';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
  private readonly users = [
    {
      id: 1,
      email: 'admin@mail.com',
      password: 'pass@1234',
    },
    {
      id: 2,
      email: 'user1@mail.com',
      password: 'pass@1234',
    },
  ];

  async findOne(email: string): Promise<User | undefined> {
    return this.users.find((user) => user.email === email);
  }
}
